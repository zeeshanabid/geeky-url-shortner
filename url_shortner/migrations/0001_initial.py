# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Word',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('word', models.CharField(unique=True, max_length=50)),
                ('url', models.CharField(max_length=500)),
                ('available', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(verbose_name='created at')),
                ('updated_at', models.DateTimeField(verbose_name='updated at')),
            ],
        ),
        migrations.AlterIndexTogether(
            name='word',
            index_together=set([('available', 'updated_at')]),
        ),
    ]
