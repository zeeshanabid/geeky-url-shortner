from django.shortcuts import render
from django.shortcuts import redirect
from url_shortner.models import Word
from django.core.exceptions import ValidationError


def index(request):
    return render(request, 'url_shortner/index.html')


def shorten(request):
    url = request.POST.get('url', '')
    if len(url) == 0:
        raise ValidationError('parameter "url" must be present and it must not contain an empty string')

    word = Word.shorten_url(url)
    context = {'url': request.build_absolute_uri('/' + word.word)}
    return render(request, 'url_shortner/shorten.html', context)


def redirect_url(request, word):
    word_url = Word.objects.filter(word=word).first()
    return redirect(word_url.url, permanent=False)
