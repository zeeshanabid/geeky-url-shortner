from django.test import TestCase
from django.test.utils import setup_test_environment
from django.test import Client
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.utils.six import StringIO
from url_shortner.models import Word


class WordViewTestCase(TestCase):
    def setUp(self):
        setup_test_environment()
        out = StringIO()
        call_command('load_words', './url_shortner/sample_words.txt', stdout=out)

    def test_index_page(self):
        """Testing index page"""
        client = Client()
        response = client.get('/')
        self.assertContains(response, "Geeky URL shortening service", status_code=200)

    def test_shorten_words(self):
        """Testing shorten words functionality"""
        client = Client()
        self.assertRaises(ValidationError, client.get, '/short/url')

        response = client.post('/short/url', {'url': 'http://bbc.com/news'})
        self.assertEqual(200, response.status_code, 'Must return 200 status code')
        self.assertContains(response, "Geeky URL shortening service", status_code=200)
        self.assertContains(response, "/book", status_code=200)

    def test_url_redirect(self):
        """Testing URL redirection"""

        client = Client()
        url = 'http://bbc.com/news'
        word_url = Word.shorten_url(url)
        response = client.get('/' + word_url.word + '/')
        self.assertEqual(response.status_code, 302, "Must do a temporary redirect")
