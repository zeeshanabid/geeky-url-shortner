from django.db import models
from django.db.models.signals import pre_save
from django.utils import timezone
import re


class Word(models.Model):
    word = models.CharField(max_length=50, unique=True)
    url = models.CharField(max_length=500)
    available = models.BooleanField(default=True)
    created_at = models.DateTimeField('created at')
    updated_at = models.DateTimeField('updated at')

    NORMALIZE_REGEX = re.compile('[^A-Za-z0-9]+')

    def __str__(self):
        return 'word:{0}, url:{1}, available:{2}'.format(self.word, self.url, self.available)

    @staticmethod
    def shorten_url(url):
        url_words = Word.NORMALIZE_REGEX.sub(',', url).split(',')
        word = Word.objects.filter(word__in=url_words, available=True).first()
        if word is None:
            word = Word.objects.order_by('-available', 'updated_at').first()
        word.url = url
        word.save()
        return word

    @staticmethod
    def normalize_word(word):
        word = word.strip().lower()
        word = Word.NORMALIZE_REGEX.sub('', word)
        return word

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        instance.word = Word.normalize_word(instance.word)
        if instance.created_at is None:
            instance.created_at = timezone.now()
        if len(instance.url) > 0:
            instance.available = False
        instance.updated_at = timezone.now()

    class Meta:
        index_together = ('available', 'updated_at')

pre_save.connect(Word.pre_save, Word, dispatch_uid="url_shortner.models.Word")