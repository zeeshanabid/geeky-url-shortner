from django.test import TestCase
from django.db.utils import IntegrityError
from url_shortner.models import Word
from django.core.management import call_command
from django.utils.six import StringIO


class WordTestCase(TestCase):
    def test_saving_word_in_database(self):
        """Saving normalized word in database"""

        w = Word(word='book')
        w.save()
        self.assertEqual(w.word, 'book', 'The word must have been normalized to "book"')
        self.assertEqual(w.available, True, 'The word must be available for URL shortening')

        w = Word(word='PYTHON')
        w.save()
        self.assertEqual(w.word, 'python', 'The word must have been normalized to "python"')
        self.assertEqual(w.available, True, 'The word must be available for URL shortening')

        w = Word(word='database', url='http://localhost/database')
        w.save()
        self.assertEqual(w.word, 'database', 'The word must have been normalized to "database"')
        self.assertEqual(w.available, False, 'The word must not be available for URL shortening')

        w = Word(word='book')
        self.assertRaises(IntegrityError, w.save, "Must throw an error when saving the word which already exists")

    def test_shorten_url(self):
        """Testing URL shortening"""

        out = StringIO()
        call_command('load_words', './url_shortner/sample_words.txt', stdout=out)
        self.assertIn('Successfully loaded 5 words from file "./url_shortner/sample_words.txt"', out.getvalue())

        url = 'http://google.com/search/book'
        w = Word.shorten_url(url)
        self.assertEqual(w.url, url, 'URL must be shorten to "book" and saved in database')
        self.assertEqual(w.word, 'book', 'URL must be shorten to "book" and saved in database')
        self.assertEqual(w.available, False, 'Word must not be available anymore for URL shortening')

        url = 'http://google.com/search/python'
        w = Word.shorten_url(url)
        self.assertEqual(w.url, url, 'URL must be shorten to "python" and saved in database')
        self.assertEqual(w.word, 'python', 'URL must be shorten to "python" and saved in database')
        self.assertEqual(w.available, False, 'Word must not be available anymore for URL shortening')

        url = 'http://bing.com/search/python'
        w = Word.shorten_url(url)
        self.assertEqual(w.url, url, 'URL must be shorten to "dictionary" and saved in database')
        self.assertEqual(w.word, 'dictionary', 'URL must be shorten to "dictionary" and saved in database')
        self.assertEqual(w.available, False, 'Word must not be available anymore for URL shortening')

        url = 'http://bing.com/search/hello'
        w = Word.shorten_url(url)
        self.assertEqual(w.url, url, 'URL must be shorten to "1st" and saved in database')
        self.assertEqual(w.word, '1st', 'URL must be shorten to "1st" and saved in database')
        self.assertEqual(w.available, False, 'Word must not be available anymore for URL shortening')

        url = 'http://bing.com/search/world'
        w = Word.shorten_url(url)
        self.assertEqual(w.url, url, 'URL must be shorten to "2nd" and saved in database')
        self.assertEqual(w.word, '2nd', 'URL must be shorten to "2nd" and saved in database')
        self.assertEqual(w.available, False, 'Word must not be available anymore for URL shortening')

        # Use the oldest word if all the words are used
        url = 'http://bing.com/search/python'
        w = Word.shorten_url(url)
        self.assertEqual(w.url, url, 'URL must be shorten to "book" and saved in database when th')
        self.assertEqual(w.word, 'book', 'URL must be shorten to "2nd" and saved in database')
        self.assertEqual(w.available, False, 'Word must not be available anymore for URL shortening')
