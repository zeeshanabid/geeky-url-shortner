from django.core.management import call_command
from django.test import TestCase
from django.utils.six import StringIO
from url_shortner.models import Word


class WordLoaderTestCase(TestCase):
    def test_load_words(self):
        """Testing load words command"""
        out = StringIO()

        call_command('load_words', './url_shortner/sample_words.txt', stdout=out)
        self.assertIn('Successfully loaded 5 words from file "./url_shortner/sample_words.txt"', out.getvalue())
        self.assertEqual(5, Word.objects.count(), "Database must have 5 words")

        out = StringIO()
        call_command('load_words', './url_shortner/sample_words.txt', stdout=out)
        self.assertIn('Successfully loaded 0 words from file "./url_shortner/sample_words.txt"', out.getvalue())
        self.assertEqual(5, Word.objects.count(), "Database must have 5 words")

        ex_words = ['book', 'dictionary', '1st', '2nd', 'python']
        w = lambda x: x.word
        db_words = Word.objects.all()
        self.assertEqual(ex_words, list(map(w,db_words)), "Words in database match the array")

