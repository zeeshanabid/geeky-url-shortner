from django.core.management.base import BaseCommand, CommandError
from url_shortner.models import Word


class Command(BaseCommand):
    help = 'Loads a list of words from a text file'

    def add_arguments(self, parser):
        parser.add_argument('file_paths', nargs='+', type=str)

    def handle(self, *args, **options):
        count = 0
        for file_path in options['file_paths']:
            words_file = open(file_path)
            for w in words_file:
                if not Word.objects.filter(word=Word.normalize_word(w)).exists():
                    word = Word(word=w)
                    word.save()
                    count += 1
            self.stdout.write('Successfully loaded {} words from file "{}"'.format(count, file_path))
