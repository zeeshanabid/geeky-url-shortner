"""geeky_url_shortner URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from url_shortner import views as shorten_url_views

urlpatterns = [
    url(r'', include('url_shortner.urls')),
    url(r'short/url', shorten_url_views.shorten),
    url(r'^admin/login', include(admin.site.urls)),
    url(r'^(?P<word>[a-zA-Z0-9]+)/$', include('url_shortner.urls')),
]
